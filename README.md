# Namibia Census 2021

## Current Versions

* CSPro 7.5.1 11 December 2020
* CSEntry 7.5.1 11 December 2020
* CSWeb 7.5.0 08 October 2020

## Deployment

Deploying the project is a critical part of the census. You must understand and test this process. A mistake at this point could have serious consequences for the success of the census.

### Location URLs that need to point to server to deploy

* */Deploy Tools/Deploy - Remote.csds*
* */Shared Logic/Server.apc*
  * Used by *Upload Lookup Data* batch application
  * Used by *Menu* CAPI application

### Clean deployment

**WARNING** **DANGER** Only do this before deploying any devices. Otherwise, this will create duplicates**

* Build machine - delete "*Deploy - Namibia Census*" and *Data* folders
* CSWeb - delete each dictionary in the data dashboard (this deletes data also)
* **ALL** Mobile devices - delete "*Deploy - Namibia Census*" folder

### Deploy to devices

* Open *Build All.pffRunner* and press run. This will do the following:
  * Update each reference by running each *XL2CS* with the *Modify, add, delete cases*
  * *Deploy - Remote.pff* - Uploads all dictionaries used in synchronization to CSWeb (also uploads app, but is ignored)
  * *Upload Lookup Data.pff* - Upload lookup data (assignments, geocodes, listing, staff) to CSWeb
  * *Deploy - Local.pff* - Builds the final project
    * Copy "Deploy - Namibia Census" to server (csweb/files/)
    * Copy "Deploy - Namibia Census" to mobile devices (csentry/)
* Copy "Tile Packages" to mobile devices (csentry/Deploy - Namibia Census/)

### Explanation

All new lookup data should be added to the system through the Excel reference files. If the references are built without the setting *Modify, add, delete cases* the CSDB files will be deleted and rebuilt. This results in new GUIDs being associated with each case. This will cause duplicates in the lookup files when synchronization occurs. Manually deleting the lookup CSDB files have the same negative effect.

Further, consider what could happen if the tablets and server were set up in advance of the census. Then for whatever reason, someone ran this script from another computer. The CSDB files would be rebuilt with new GUILDs and uploaded to the server. The tablets would have CSDB files with different GUIDs and when a synchronization occurs in the field all lookup files would have duplicates.

The *Deploy - Remote.pff* and *Upload Lookup Data.pff* are run before *Deploy - Local.pff,* so lookup files have a sync history that includes an upload to the server. This will allow the first synchronization in the field avoid downloadind all lookup data.

Note that the TPK files are not deployed as part of this process. They are large and you'll likely need to develop a plan to load only a portion of the country's TPKs on each tablet.

## Usage

### Maps

#### Icons

* Add mode
  * **Red exclamation** (not started): No household in structure have been enumerated
  * **Orange hourglass** (in progress): At least one household is not started and another is either complete or partial
  * **Green checkmark** (complete): All households within structure are complete

* Restore mode
  * **Grey circle ban** (deleted): Any structure that has been deleted

#### Structures on the map

Each structure is displayed on the map with a single marker. Dwellings and households in the structure are managed by opening the structure menu. If the structure is defined in the listing then the GPS from the listing is used. If the GPS is missing from the listing then the house will not be enumerable. Alternatively, the structure can be added directly to the map. In that case the GPS for the marker will be used. When the **first** household inside the structure is enumerated the GPS for the household will be used for the structure. Also, the name of the head of household from the **first** household will be used in the structure description.

#### Adding structures

Press add button and then tap location of marker. Retap to resposition marker as necessary. Press done and the marker will be placed and structure menu is opened.  

#### Deleting structures, dwellings, and households

Tap structure then description to open structure menu. The current structure, and dwellings and households inside the current structure can be deleted. Structures created at headquarters or in the field can be deleted. However, only dwellings and households created in the field can be deleted.

#### Restoring structures, dwellings, and households

Press restore button and then tap structure. Confirm restoration and structure menu is opened. Dwellings and households for the current structure can be restored.

### Multiple interviewers in an EA

The variable A_INTERVIEWER_CODE in the assignments guarantees all enumerations are unique. However, all interviewers assigned to EA will see all structures. It is the responsibility of the supervisor to make sure the interviewers are not enumerating the same structures.

### Working with Assignments

#### Interviewer

Interviewers will only see work (structures, dwellings, and households) for their current assignment. They'll need to switch assignments to work in another EA.

#### Supervisor

Supervisors will see all interviewers assigned to them regardless of the supervisor's current assignment. However, the supervisor will only be able to update interviewer assignments for EAs within the supervisor's current assignment. E.g. Intvw1 has an assignment in region 1 constituency 1 EA 1 and the supervisor's current assignment is region 1 constituency 2. The supervisor will be able to add an assignment for their current assignment, but to delete the Intvw1's assignment they will need switch their assignment to region 1 consituency 1.

#### Headquarters

Supervisors do not have the ability to assign themselves new interviewers. HQ must add the assignment in this case.

### Managing structures, dwelling units, and households

The structure, dwelling unit, and households are defined in the listing.

* Valid hq structures are 1 - 5,000
* Valid hq dwelling units are 1 - 5,000
* Valid hq households are 1 - 500

Once fieldwork begins the listing cannot be changed. All further changes will occur in the field and be written to the listing overrride files.

* Valid user-created structures are 5,001 - 9,999
* Valid user-created dwelling units are 5,001 - 9,999
* Valid user-created households are 501 - 999

## Dictionaries

### WORK_DICT

The listing frame could be for the entire country, so the work dictionary is populated with each household in the current ea (i.e. the work). Now instead of looping through the entire country's households to create a dynamic value set or generate a report we can loop through the work which will be faster. When the interviewer switches assignments the work will be repopulated. If the interviewer deletes or adds a household the work will be repopulated. However, other actions like logging in will not trigger its repopulation.

### OVERRIDE_ASSIGNMENTS_DICT

The ASSIGNMENTS_DICT is created at headquarters. It can only be modified by headquarter's staff and downloaded via synchronization. The OVERRIDE_ASSIGNMENTS_DICT allows supervisors to override what is in ASSIGNMENTS_DICT without writing to it. If the supervisor assigns an interviewer a new case the OVERRIDE_ASSIGNMENTS_DICT will contain this assignment and a status of "A" (assigned). Otherwise, if the supervisor deletes an assignment for an interviewer the OVERRIDE_ASSIGNMENTS_DICT will contain the deleted assignment with a status of "D" (deleted). When creating a dynamic value set of the interviewer's assignments the application developer will need to first loop through the ASSIGNMENTS_DICT. For each assignment assigned to the interviewer check in the OVERRIDE_ASSIGNMENTS_DICT if the status is "D." If it is than the assignment can be ignored, otherwise include it in the value set. Afterr looping through the ASSIGNMENTS_DICT, then loop through OVERRIDE_ASSIGNMENTS_DICT. For each assignment assigned to the interviewer check if the status is "A." If it is than add it to the value set, otherwise ignore it.

### LISTING_OVERRIDE_STRUCTURE_DICT, LISTING_OVERRIDE_DWELLING_DICT, LISTING_OVERRIDE_HOUSEHOLD_DICT

The use of the listing overrides is very similar to the assignments override. The LISTING_DICT is created at headquarts. It can only be modified by headquarter's staff and downloaded via synchronization. The listing overrides allow interviewers to add or delete a structure, dwelling, or household. Like the assignments override they include a status that is either "A" (assigned) or "D" (deleted). It also includes a source which indicates whether it was added at headquarters (HQ) or in the field (FS). The latitude and longitude are currently only used for the structure, but exist in all three dictionaries.

The dictionaries are kept in sync top down and bottom up. For instance, when a structure is deleted, all dwellings and structures within it are deleted. If a household is deleted and it is the last household within the dwelling, then the dwelling is deleted. If that dwelling is the last dwelling in the structure than the structure is deleted.

### WS_DICT

The working storage is used to generate templated reports.
